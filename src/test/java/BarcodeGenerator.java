import java.io.File;

import java.io.FileOutputStream;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;

// References.
// https://www.youtube.com/watch?v=txmyS88iQHs
// https://downloads.aspose.com/barcode/java
// https://charli105patel.wordpress.com/2013/03/11/generate-barcode-using-zxing-library-in-java/
// https://mvnrepository.com/artifact/com.google.zxing/javase
//https://mvnrepository.com/artifact/com.google.zxing/core

public class BarcodeGenerator {

//	private static String strBaseFolder = "./src/";

	
//		The following code can write QRcode
//		String text = "98376373783"; // this is the text that we want to encode  
//		int width = 400;  
//		int height = 300; // change the height and width as per your requirement  
		// (ImageIO.getWriterFormatNames() returns a list of supported formats)  
//		String imageFormat = "png"; // could be "gif", "tiff", "jpeg"   
//		BitMatrix bitMatrix = new QRCodeWriter().encode(text, BarcodeFormat.QR_CODE, width, height);  
//		MatrixToImageWriter.writeToStream(bitMatrix, imageFormat, new FileOutputStream(new File("qrcode_97802017507991.png")));

	// The Following code can write barcode
			//  Write Barcode
	public	File generateBarcode(String uniqueID) {
		try {
		//BitMatrix bitMatrix = new Code128Writer().encode(uniqueID, BarcodeFormat.CODE_128, 200, 100, null);
		BitMatrix bitMatrix = new Code128Writer().encode(uniqueID, BarcodeFormat.CODE_128, 400, 200, null);
		MatrixToImageWriter.writeToStream(bitMatrix, "png", new FileOutputStream(new File("./src/Barcode"+uniqueID+".png")));
		File barcode =  new File("./src/Barcode"+uniqueID+".png");
		return barcode;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return new File("./src/Barcode.png");
	}
		

		
		
		
		
		
	
}

import java.util.Vector;

public class Customer {
	private String name;
	private String customerID;
	private Vector<Token> tokens;
	
	public Customer(String name) {
		tokens = new Vector<>();
		String now = Long.toString(System.currentTimeMillis());
		
		this.name = name;		
		this.setCustomerID(Token.hash(this.name + now, "salty"));
	}	
	
	public Customer() {
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCustomerID() {
		return customerID;
	}

	public void setCustomerID(String customerID) {
		this.customerID = customerID;
	}

	public Vector<Token> getTokens() {
		return tokens;
	}

	public void setTokens(Vector<Token> tokens) {
		this.tokens = tokens;
	}
}

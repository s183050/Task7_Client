import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.junit.Test;

public class SimulateRequestClientTest {
	//@Test public void testTokensTestResource
//	{
//		Client c = ClientBuilder.newClient();
//		WebTarget r = c.target("http://02267-tokyo.compute.dtu.dk:8282");
//
//		Customer customer = new Customer("name");
//		String request = r.path("tokens").queryParam("number", 2).request()
//				.post(Entity.entity(customer, MediaType.APPLICATION_JSON), String.class);
//		assertEquals("Hello there!");
//	}

	@Test 
	public void testTokensTestResource()
	{
		Client c = ClientBuilder.newClient();
		WebTarget r = c.target("http://02267-tokyo.compute.dtu.dk:8181");

		Customer customer = new Customer("Jon Snow");
		System.out.println("Customer: + \n" + "Name: " + customer.getName() + "\n" + "ID: " + customer.getCustomerID());
		
		Response request =  r.path("tokens").path("request").queryParam("number", 3).request()
				.post(Entity.json(customer));
		
		String requestResponse = request.readEntity(String.class);
		request.close();
		
		System.out.println("Server response 1: " +  requestResponse);

		//assertTrue(requestResponse);
		assertEquals(requestResponse.substring(0,4),"true");
		
		
		Response request2 =  r.path("tokens").path("request").queryParam("number", 1).request()
				.post(Entity.json(customer));
		
		String requestResponse2 = request2.readEntity(String.class);
		request2.close();
		
		System.out.println("Server response 2: " +  requestResponse2);

		assertEquals(requestResponse2.substring(0,5),"false");
		
		//assertFalse(requestResponse2);
	}

//	String jsonFile = "EMPTY";
//	ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
//	try {
//		jsonFile = ow.writeValueAsString(customer);
//	} catch (JsonProcessingException e) {
//		e.printStackTrace();
//	}
}
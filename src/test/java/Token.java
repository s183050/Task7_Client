
import java.io.File;
import java.security.MessageDigest;

public class Token {
	String tokenID;
	File barcode;	
	BarcodeGenerator barcodeGenerator;
	
	
	public Token() {
		String now = Long.toString(System.currentTimeMillis());
		barcodeGenerator = new BarcodeGenerator();
		
		this.tokenID = hash(now, "salty");
		this.barcode = barcodeGenerator.generateBarcode(tokenID);		
	}
	
    public static String hash(String password, String salt) {
    	try {
        MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
        StringBuilder sb = new StringBuilder();
        byte[] passBytes = (password + salt).getBytes();
        byte[] passHash = sha256.digest(passBytes);
        
        for(int i=0; i< passHash.length ;i++) {
            sb.append(Integer.toString((passHash[i] & 0xff) + 0x100, 16).substring(1));
        }
        String generatedPassword = sb.toString();
    	
        return generatedPassword;
    	}
    	catch(Exception e){
    		e.printStackTrace();
    	}
    	return "Hash_error";
    }
    
    
    public String getTokenID() {
		return tokenID;
	}
	public void setTokenID(String id) {
		tokenID=id;
	}
	
	public File getBarcode() {
		return barcode;
	}
	
	public void setBarcode(File b) {
		barcode=b;
	}
	
	public BarcodeGenerator getBarcodeGenerator() {
		return barcodeGenerator;
	}
	public void setBarcodeGenerator(BarcodeGenerator bg) {
		barcodeGenerator=bg;
	}
	
}
